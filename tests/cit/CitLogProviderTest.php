<?php 

namespace Solides\Business\Logs\Tests;

use PHPUnit\Framework\TestCase;
use Solides\Business\Logs\Providers\cit\CitLogProvider;



class CitLogProviderTest extends TestCase {

    /**
     * Summary of testExperimetn
     * @return void
     */
    public function testSetAcao()
    {   
        $log = new CitLogProvider();
        $expected = 'cancelamento_venda_teste';
        $log->setAcao($expected);  
        $actual = $log->getAcao();

        self::assertIsString($actual);
        // self::assertEquals($expected, $actual);
        
    }

    public function testSetInfoMsg()
    {   
        $log = new CitLogProvider();
        $expected = 'Processado com sucesso. - Inseriu prospeccao: 713538; Inseriu venda: 717581; Inseriu venda item: (IDVENDA: 717581, IDPROSP: 713538, IDPROD: 172);  Cliente já existente no Omie nCodCli: 2372390866';
        $log->setInfoMsg($expected);  
        $actual = $log->getInfoMsg();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    
    }

    public function testSetSetor()
    {   
        $log = new CitLogProvider();
        $expected = 'financeiro';
        $log->setSetor($expected);  
        $actual = $log->getSetor();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetRequestFrom()
    {   
        $log = new CitLogProvider();
        $expected = 'CIT';
        $log->setRequestFrom($expected);  
        $actual = $log->getRequestFrom();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetIdClifor()
    {   

        $log = new CitLogProvider();
        $expected = 182;
        $log->setIdClifor($expected);  
        $actual = $log->getIdClifor();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetDealId()
    {   
        $log = new CitLogProvider();
        $expected = 182;
        $log->setDealId($expected);  
        $actual = $log->getDealId();

        self::assertIsInt($actual);
        self::assertEquals($expected, $actual);
    }

    /**
     * Summary of testSetIdGestao
     * @return void
     */
    public function testSetIdGestao()
    {   
        $log = new CitLogProvider();
        $expected = 12;
        $log->setIdGestao($expected);  
        $actual = $log->getIdGestao();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetStatus()
    {   
        $log = new CitLogProvider();
        $expected = 'sucesso';
        $log->setStatus($expected);  
        $actual = $log->getStatus();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetHubspotOwnerId()
    {   
        $log = new CitLogProvider();
        $expected = 37370045;
        $log->setHubspotOwnerId($expected);  
        $actual = $log->getHubspotOwnerId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetDisparadoPor()
    {   
        $log = new CitLogProvider();
        $expected = 'walyson.solides@gmail.com.br';
        $log->setDisparadoPor($expected);  
        $actual = $log->getDisparadoPor();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }
    

    public function testSetCurlInfo()
    {   

        $log = new CitLogProvider();
        $expected = [
            'curl_info' => '{
                "url": "https:\/\/app.omie.com.br\/api\/v1\/servicos\/os\/",
                "content_type": "application\/json; encoding=UTF-8",
                "http_code": 200,
                "header_size": 648,
                "request_size": 186,
                "filetime": -1,
                "ssl_verify_result": 0,
                "redirect_count": 0,
                "total_time": 2.450945,
                "namelookup_time": 0.005634,
                "connect_time": 0.031369,
                "pretransfer_time": 0.099186,
                "size_upload": 1282,
                "size_download": 149,
                "speed_download": 60,
                "speed_upload": 523,
                "download_content_length": 149,
                "upload_content_length": 1282,
                "starttransfer_time": 0.121617,
                "redirect_time": 0,
                "redirect_url": "",
                "primary_ip": "18.231.66.213",
                "certinfo": [],
                "primary_port": 443,
                "local_ip": "192.168.0.104",
                "local_port": 37686
            }'
        ];
        $log->setCurlInfo($expected);  
        $actual = $log->getCurlInfo();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);

    }

    public function testSetPayload()
    {   
        $log = new CitLogProvider();
        $expected = [
            'payload' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ];
        $log->setPayload($expected);  
        $actual = $log->getPayload();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetResponse()
    {   
        $log = new CitLogProvider();
        $expected = [
            'response' => '{
            "cCodIntOS": "172#avulso#C#1649681533",
            "nCodOS": 2373993367,
            "cNumOS": "000000000000600",
            "cCodStatus": "0",
            "cDescStatus": "Ordem de Serviço adicionada com sucesso!",
            "payload": {
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ];
        $log->setResponse($expected);  
        $actual = $log->getResponse();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetCreatedAt()
    {   
        $log = new CitLogProvider();
        $expected = '2022-04-11 09:52:12';
        $log->setCreatedAt($expected);  
        $actual = $log->getCreatedAt();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetHttpCode()
    {   
        $log = new CitLogProvider();
        $expected = 'http://locahost:8998';
        $log->setHttpCode($expected);  
        $actual = $log->getHttpCode();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
        
    }
    public function testGetAcao()
    {   
        $log = new CitLogProvider();

        $expected = 'cancelamento';
        $log->setAcao('cancelamento');
        $actual = $log->getAcao();

        self::assertIsString($actual);
        // self::assertEquals($expected, $actual);
    }

    public function testgetInfoMsg()
    {   
        $log = new CitLogProvider();

        $expected = 'Processado com sucesso. - Inseriu prospeccao: 713538; Inseriu venda: 717581; Inseriu venda item: (IDVENDA: 717581, IDPROSP: 713538, IDPROD: 172);  Cliente já existente no Omie nCodCli: 2372390866';
        $log->setInfoMsg('Processado com sucesso. - Inseriu prospeccao: 713538; Inseriu venda: 717581; Inseriu venda item: (IDVENDA: 717581, IDPROSP: 713538, IDPROD: 172);  Cliente já existente no Omie nCodCli: 2372390866');
        $actual = $log->getInfoMsg();


        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetSetor()
    {   
        $log = new CitLogProvider();

        $expected = 'financeiro';
        $log->setSetor('financeiro');
        $actual = $log->getSetor();


        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetRequestFrom()
    {   
        $log = new CitLogProvider();

        $expected = 'CIT';
        $log->setRequestFrom('CIT');
        $actual = $log->getRequestFrom();


        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }
    public function testgetIdClifor()
    {   
        $log = new CitLogProvider();

        $expected = 183;
        $log->setIdClifor(183);
        $actual = $log->getIdClifor();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetHttpCode()
    {   
        $log = new CitLogProvider();

        $expected = 'http://locahost:8998';
        $log->setHttpCode('http://locahost:8998');
        $actual = $log->getHttpCode();


        self::assertIsString($actual);
        self::assertEquals($expected, $actual);

    }

    public function testGetDealId()
    {   
        $log = new CitLogProvider();
        $expected = 182;
        $log->setDealId(182);  
        $actual = $log->getDealId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);

    }

    public function testGetIdGestao()
    {   
        $log = new CitLogProvider();
        $expected = 12;
        $log->setIdGestao(12);  
        $actual = $log->getIdGestao();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetStatus()
    {   
        $log = new CitLogProvider();
        $expected = 'sucesso';
        $log->setStatus('sucesso');  
        $actual = $log->getStatus();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetHubspotOwnerId()
    {
        $log = new CitLogProvider();
        $expected = 37370045;
        $log->setHubspotOwnerId(37370045);  
        $actual = $log->getHubspotOwnerId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetDisparadoPor()
    {
        $log = new CitLogProvider();
        $expected = 'walyson.solides@gmail.com.br';
        $log->setDisparadoPor('walyson.solides@gmail.com.br');  
        $actual = $log->getDisparadoPor();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetCurlInfo()
    {
        $log = new CitLogProvider();
        $expected = [
            'curl_info' => '{
                "url": "https:\/\/app.omie.com.br\/api\/v1\/servicos\/os\/",
                "content_type": "application\/json; encoding=UTF-8",
                "http_code": 200,
                "header_size": 648,
                "request_size": 186,
                "filetime": -1,
                "ssl_verify_result": 0,
                "redirect_count": 0,
                "total_time": 2.450945,
                "namelookup_time": 0.005634,
                "connect_time": 0.031369,
                "pretransfer_time": 0.099186,
                "size_upload": 1282,
                "size_download": 149,
                "speed_download": 60,
                "speed_upload": 523,
                "download_content_length": 149,
                "upload_content_length": 1282,
                "starttransfer_time": 0.121617,
                "redirect_time": 0,
                "redirect_url": "",
                "primary_ip": "18.231.66.213",
                "certinfo": [],
                "primary_port": 443,
                "local_ip": "192.168.0.104",
                "local_port": 37686
            }'
        ];
        $log->setCurlInfo([
            'curl_info' => '{
                "url": "https:\/\/app.omie.com.br\/api\/v1\/servicos\/os\/",
                "content_type": "application\/json; encoding=UTF-8",
                "http_code": 200,
                "header_size": 648,
                "request_size": 186,
                "filetime": -1,
                "ssl_verify_result": 0,
                "redirect_count": 0,
                "total_time": 2.450945,
                "namelookup_time": 0.005634,
                "connect_time": 0.031369,
                "pretransfer_time": 0.099186,
                "size_upload": 1282,
                "size_download": 149,
                "speed_download": 60,
                "speed_upload": 523,
                "download_content_length": 149,
                "upload_content_length": 1282,
                "starttransfer_time": 0.121617,
                "redirect_time": 0,
                "redirect_url": "",
                "primary_ip": "18.231.66.213",
                "certinfo": [],
                "primary_port": 443,
                "local_ip": "192.168.0.104",
                "local_port": 37686
            }'
        ]);  
        $actual = $log->getCurlInfo();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetPayload()
    {
        $log = new CitLogProvider();
        $expected = [
            'payload' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ];
        $log->setPayload([
            'payload' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ]);  
        $actual = $log->getPayload();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetResponse()
    {
        $log = new CitLogProvider();
        $expected = [
            'response' => '{
            "cCodIntOS": "172#avulso#C#1649681533",
            "nCodOS": 2373993367,
            "cNumOS": "000000000000600",
            "cCodStatus": "0",
            "cDescStatus": "Ordem de Serviço adicionada com sucesso!",
            "payload": {
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ];
        $log->setResponse([
            'response' => '{
            "cCodIntOS": "172#avulso#C#1649681533",
            "nCodOS": 2373993367,
            "cNumOS": "000000000000600",
            "cCodStatus": "0",
            "cDescStatus": "Ordem de Serviço adicionada com sucesso!",
            "payload": {
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ]);  
        $actual = $log->getResponse();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetCreatedAt()
    {
        $log = new CitLogProvider();
        $expected = '2022-04-11 09:52:12';
        $log->setCreatedAt('2022-04-11 09:52:12');  
        $actual = $log->getCreatedAt();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetLogsPush(){

        $log = new CitLogProvider();

        $expected = [
                'acao' => 'cancelamento_venda_teste',
                'info_msg' => 'Processado com sucesso. - Inseriu prospeccao: 713538; Inseriu venda: 717581; Inseriu venda item: (IDVENDA: 717581, IDPROSP: 713538',
                'setor' => 'Financeiro',
                'request_from' => 'CIT',
                'idclifor' => '182',
                'deal_id' => 'NULL',
                'id_gestao' => 'NULL',
                'status' => 'success',
                'hubspot_owner_id' => 'NULL',
                'disparado_por' => 'geraldo.junior@solides.com.br',
                'curl_info' => '{
                    "url": "https:\/\/app.omie.com.br\/api\/v1\/servicos\/os\/",
                    "content_type": "application\/json; encoding=UTF-8",
                    "http_code": 200,
                    "header_size": 648,
                    "request_size": 186,
                    "filetime": -1,
                    "ssl_verify_result": 0,
                    "redirect_count": 0,
                    "total_time": 2.450945,
                    "namelookup_time": 0.005634,
                    "connect_time": 0.031369,
                    "pretransfer_time": 0.099186,
                    "size_upload": 1282,
                    "size_download": 149,
                    "speed_download": 60,
                    "speed_upload": 523,
                    "download_content_length": 149,
                    "upload_content_length": 1282,
                    "starttransfer_time": 0.121617,
                    "redirect_time": 0,
                    "redirect_url": "",
                    "primary_ip": "18.231.66.213",
                    "certinfo": [],
                    "primary_port": 443,
                    "local_ip": "192.168.0.104",
                    "local_port": 37686
                }',
                'payload' => '{
                    "Cabecalho": {
                        "cCodIntOS": "172#avulso#C#1649681533",
                        "cEtapa": "30",
                        "dDtPrevisao": "12\/04\/2022",
                        "nCodCli": 2372390866,
                        "nQtdeParc": 1,
                        "cCodParc": "000"
                    }',
                'response' => '{
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "nCodOS": 2373993367,
                    "cNumOS": "000000000000600",
                    "cCodStatus": "0",
                    "cDescStatus": "Ordem de Serviço adicionada com sucesso!",
                    "payload": {
                        "Cabecalho": {
                            "cCodIntOS": "172#avulso#C#1649681533",
                            "cEtapa": "30",
                            "dDtPrevisao": "12\/04\/2022",
                            "nCodCli": 2372390866,
                            "nQtdeParc": 1,
                            "cCodParc": "000"
                        }',
                'created_at' => '2022-04-11 09:52:12',
                'http_code' => 'http://locahost:8998',

            ];
        $log->setLogsPush($expected);   
        $actual = $expected;

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);         
    }

}