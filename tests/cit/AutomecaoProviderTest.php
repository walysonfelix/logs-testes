<?php 

namespace Solides\Business\Logs\Tests;

use PHPUnit\Framework\TestCase;
use Solides\Business\Logs\Providers\cit\AutomecaoProvider;



class AutomecaoProviderTest extends TestCase {


     /**
     * @var integer
     */
    private ?int $idOs = null;
    

     /**
     * Summary of testExperimetn
     * @return void
     */

    public function testSetAcao()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'edit_venda_item';
        $auto->setAcao($expected);  
        $actual = $auto->getAcao();

        self::assertIsString($actual);
        // self::assertEquals($expected, $actual);
        
    }

    public function testSetStatus()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'sucesso';
        $auto->setStatus($expected);  
        $actual = $auto->getStatus();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetInfoMsg()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'Processado com sucesso. - Inseriu prospeccao: 713538; Inseriu venda: 717581; Inseriu venda item: (IDVENDA: 717581, IDPROSP: 713538, IDPROD: 172);  Cliente já existente no Omie nCodCli: 2372390866';
        $auto->setInfoMsg($expected);  
        $actual = $auto->getInfoMsg();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    
    }

    public function testSetSetor()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'financeiro';
        $auto->setSetor($expected);  
        $actual = $auto->getSetor();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }


    public function testSetRequestFrom()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'CIT';
        $auto->setRequestFrom($expected);  
        $actual = $auto->getRequestFrom();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetTarget()
    {   
        $auto = new AutomecaoProvider();
        $expected = 185;
        $auto->setTarget($expected);  
        $actual = $auto->getTarget();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetIdClifor()
    {   

        $auto = new AutomecaoProvider();
        $expected = 182;
        $auto->setIdClifor($expected);  
        $actual = $auto->getIdClifor();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetDealId()
    {   
        $auto = new AutomecaoProvider();
        $expected = 182;
        $auto->setDealId($expected);  
        $actual = $auto->getDealId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetCompanyId()
    {   
        $auto = new AutomecaoProvider();
        $expected = 188;
        $auto->setCompanyId($expected);  
        $actual = $auto->getCompanyId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetIdGestao()
    {   
        $auto = new AutomecaoProvider();
        $expected = 12;
        $auto->setIdGestao($expected);  
        $actual = $auto->getIdGestao();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetHubspotOwnerId()
    {   
        $auto = new AutomecaoProvider();
        $expected = 37370045;
        $auto->setHubspotOwnerId($expected);  
        $actual = $auto->getHubspotOwnerId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetCurlInfo()
    {   

        $auto = new AutomecaoProvider();
        $expected = [
            'curl_info' => '{
                "url": "https:\/\/app.omie.com.br\/api\/v1\/servicos\/os\/",
                "content_type": "application\/json; encoding=UTF-8",
                "http_code": 200,
                "header_size": 648,
                "request_size": 186,
                "filetime": -1,
                "ssl_verify_result": 0,
                "redirect_count": 0,
                "total_time": 2.450945,
                "namelookup_time": 0.005634,
                "connect_time": 0.031369,
                "pretransfer_time": 0.099186,
                "size_upload": 1282,
                "size_download": 149,
                "speed_download": 60,
                "speed_upload": 523,
                "download_content_length": 149,
                "upload_content_length": 1282,
                "starttransfer_time": 0.121617,
                "redirect_time": 0,
                "redirect_url": "",
                "primary_ip": "18.231.66.213",
                "certinfo": [],
                "primary_port": 443,
                "local_ip": "192.168.0.104",
                "local_port": 37686
            }'
        ];
        $auto->setCurlInfo($expected);  
        $actual = $auto->getCurlInfo();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);

    }


    public function testSetHttpCode()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'http://locahost:8998';
        $auto->setHttpCode($expected);  
        $actual = $auto->getHttpCode();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
        
    }

    public function testSetPayload()
    {   
        $auto = new AutomecaoProvider();
        $expected = [
            'payload' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ];
        $auto->setPayload($expected);  
        $actual = $auto->getPayload();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetResponse()
    {   
        $auto = new AutomecaoProvider();
        $expected = [
            'response' => '{
            "cCodIntOS": "172#avulso#C#1649681533",
            "nCodOS": 2373993367,
            "cNumOS": "000000000000600",
            "cCodStatus": "0",
            "cDescStatus": "Ordem de Serviço adicionada com sucesso!",
            "payload": {
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ];
        $auto->setResponse($expected);  
        $actual = $auto->getResponse();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetDisparadoPor()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'walyson.solides@gmail.com.br';
        $auto->setDisparadoPor($expected);  
        $actual = $auto->getDisparadoPor();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetMessageId()
    {   
        $auto = new AutomecaoProvider();
        $expected = 37370045;
        $auto->setMessageId($expected);  
        $actual = $auto->getMessageId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }
    
    public function testSetTattempts()
    {   
        $auto = new AutomecaoProvider();
        $expected = 23435;
        $auto->setTattempts($expected);  
        $actual = $auto->geTattempts();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetCreatedAt()
    {   
        $auto = new AutomecaoProvider();
        $expected = '2022-04-11 09:52:12';
        $auto->setCreatedAt($expected);  
        $actual = $auto->getCreatedAt();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetAcao()
    {   
        $auto = new AutomecaoProvider();

        $expected = 'cancelamentos';
        $auto->setAcao('cancelamentos');
        $actual = $auto->getAcao();


        self::assertIsString($actual);
        // self::assertEquals($expected, $actual);
    }

    public function testGetStatus()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'sucesso';
        $auto->setStatus('sucesso');  
        $actual = $auto->getStatus();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetSetor()
    {   
        $auto = new AutomecaoProvider();

        $expected = 'financeiro';
        $auto->setSetor('financeiro');
        $actual = $auto->getSetor();


        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetRequestFrom()
    {   
        $auto = new AutomecaoProvider();

        $expected = 'CIT';
        $auto->setRequestFrom('CIT');
        $actual = $auto->getRequestFrom();


        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetTarget()
    {   
        $auto = new AutomecaoProvider();
        $expected = 185;
        $auto->setTarget(185);  
        $actual = $auto->getTarget();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetIdClifor()
    {   

        $auto = new AutomecaoProvider();
        $expected = 182;
        $auto->setIdClifor(182);  
        $actual = $auto->getIdClifor();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetDealId()
    {   
        $auto = new AutomecaoProvider();
        $expected = 185;
        $auto->setDealId(185);  
        $actual = $auto->getDealId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }
    public function testGetCompanyId()
    {   
        $auto = new AutomecaoProvider();
        $expected = 188;
        $auto->setCompanyId(188);  
        $actual = $auto->getCompanyId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetIdGestao()
    {   
        $auto = new AutomecaoProvider();
        $expected = 12;
        $auto->setIdGestao(12);  
        $actual = $auto->getIdGestao();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetHubspotOwnerId()
    {   
        $auto = new AutomecaoProvider();
        $expected = 37370045;
        $auto->setHubspotOwnerId(37370045);  
        $actual = $auto->getHubspotOwnerId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetCurlInfo()
    {   

        $auto = new AutomecaoProvider();
        $expected = [
            'curl_info' => '{
                "url": "https:\/\/app.omie.com.br\/api\/v1\/servicos\/os\/",
                "content_type": "application\/json; encoding=UTF-8",
                "http_code": 200,
                "header_size": 648,
                "request_size": 186,
                "filetime": -1,
                "ssl_verify_result": 0,
                "redirect_count": 0,
                "total_time": 2.450945,
                "namelookup_time": 0.005634,
                "connect_time": 0.031369,
                "pretransfer_time": 0.099186,
                "size_upload": 1282,
                "size_download": 149,
                "speed_download": 60,
                "speed_upload": 523,
                "download_content_length": 149,
                "upload_content_length": 1282,
                "starttransfer_time": 0.121617,
                "redirect_time": 0,
                "redirect_url": "",
                "primary_ip": "18.231.66.213",
                "certinfo": [],
                "primary_port": 443,
                "local_ip": "192.168.0.104",
                "local_port": 37686
            }'
        ];
        $auto->setCurlInfo([
            'curl_info' => '{
                "url": "https:\/\/app.omie.com.br\/api\/v1\/servicos\/os\/",
                "content_type": "application\/json; encoding=UTF-8",
                "http_code": 200,
                "header_size": 648,
                "request_size": 186,
                "filetime": -1,
                "ssl_verify_result": 0,
                "redirect_count": 0,
                "total_time": 2.450945,
                "namelookup_time": 0.005634,
                "connect_time": 0.031369,
                "pretransfer_time": 0.099186,
                "size_upload": 1282,
                "size_download": 149,
                "speed_download": 60,
                "speed_upload": 523,
                "download_content_length": 149,
                "upload_content_length": 1282,
                "starttransfer_time": 0.121617,
                "redirect_time": 0,
                "redirect_url": "",
                "primary_ip": "18.231.66.213",
                "certinfo": [],
                "primary_port": 443,
                "local_ip": "192.168.0.104",
                "local_port": 37686
            }'
        ]);  
        $actual = $auto->getCurlInfo();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);

    }


    public function testGetHttpCode()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'http://locahost:8998';
        $auto->setHttpCode('http://locahost:8998');  
        $actual = $auto->getHttpCode();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
        
    }

    public function testGetPayload()
    {   
        $auto = new AutomecaoProvider();
        $expected = [
            'payload' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ];
        $auto->setPayload([
            'payload' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ]);  
        $actual = $auto->getPayload();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetResponse()
    {   
        $auto = new AutomecaoProvider();
        $expected = [
            'response' => '{
            "cCodIntOS": "172#avulso#C#1649681533",
            "nCodOS": 2373993367,
            "cNumOS": "000000000000600",
            "cCodStatus": "0",
            "cDescStatus": "Ordem de Serviço adicionada com sucesso!",
            "payload": {
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ];
        $auto->setResponse([
            'response' => '{
            "cCodIntOS": "172#avulso#C#1649681533",
            "nCodOS": 2373993367,
            "cNumOS": "000000000000600",
            "cCodStatus": "0",
            "cDescStatus": "Ordem de Serviço adicionada com sucesso!",
            "payload": {
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }',
        ]);  
        $actual = $auto->getResponse();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetDisparadoPor()
    {   
        $auto = new AutomecaoProvider();
        $expected = 'walyson.solides@gmail.com.br';
        $auto->setDisparadoPor('walyson.solides@gmail.com.br');  
        $actual = $auto->getDisparadoPor();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetMessageId()
    {   
        $auto = new AutomecaoProvider();
        $expected = 37370045;
        $auto->setMessageId(37370045);  
        $actual = $auto->getMessageId();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetTattempts()
    {   
        $auto = new AutomecaoProvider();
        $expected = 23435;
        $auto->setTattempts(23435);  
        $actual = $auto->geTattempts();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetCreatedAt()
    {   
        $auto = new AutomecaoProvider();
        $expected = '2022-04-11 09:52:12';
        $auto->setCreatedAt('2022-04-11 09:52:12');  
        $actual = $auto->getCreatedAt();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetLogsPush(){

        $auto = new AutomecaoProvider();

        $expected = [
                'acao' => 'cancelamento_venda_teste',
                'status' => 'success',
                'info_msg' => 'Processado com sucesso. - Inseriu prospeccao: 713538; Inseriu venda: 717581; Inseriu venda item: (IDVENDA: 717581, IDPROSP: 713538',
                'setor' => 'Financeiro',
                'request_from' => 'CIT',
                'target' => 185,
                'idclifor' => 182,
                'deal_id' => 'NULL',
                'company_id' => 250,
                'id_gestao' => 324,
                'hubspot_owner_id' => 'NULL',
                'curl_info' => '{
                    "url": "https:\/\/app.omie.com.br\/api\/v1\/servicos\/os\/",
                    "content_type": "application\/json; encoding=UTF-8",
                    "http_code": 200,
                    "header_size": 648,
                    "request_size": 186,
                    "filetime": -1,
                    "ssl_verify_result": 0,
                    "redirect_count": 0,
                    "total_time": 2.450945,
                    "namelookup_time": 0.005634,
                    "connect_time": 0.031369,
                    "pretransfer_time": 0.099186,
                    "size_upload": 1282,
                    "size_download": 149,
                    "speed_download": 60,
                    "speed_upload": 523,
                    "download_content_length": 149,
                    "upload_content_length": 1282,
                    "starttransfer_time": 0.121617,
                    "redirect_time": 0,
                    "redirect_url": "",
                    "primary_ip": "18.231.66.213",
                    "certinfo": [],
                    "primary_port": 443,
                    "local_ip": "192.168.0.104",
                    "local_port": 37686
                }',
                'http_code' => 'http://locahost:8998',
                'payload' => '{
                    "Cabecalho": {
                        "cCodIntOS": "172#avulso#C#1649681533",
                        "cEtapa": "30",
                        "dDtPrevisao": "12\/04\/2022",
                        "nCodCli": 2372390866,
                        "nQtdeParc": 1,
                        "cCodParc": "000"
                    }',
                'response' => '{
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "nCodOS": 2373993367,
                    "cNumOS": "000000000000600",
                    "cCodStatus": "0",
                    "cDescStatus": "Ordem de Serviço adicionada com sucesso!",
                    "payload": {
                        "Cabecalho": {
                            "cCodIntOS": "172#avulso#C#1649681533",
                            "cEtapa": "30",
                            "dDtPrevisao": "12\/04\/2022",
                            "nCodCli": 2372390866,
                            "nQtdeParc": 1,
                            "cCodParc": "000"
                        }',
                'disparado_por' => 'geraldo.junior@solides.com.br',
                'message_id' => 345,
                'attempts' => 223,
                'created_at' => '2022-04-11 09:52:12',

            ];
        $auto->setPostAutomocao($expected);   
        $actual = $expected;

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);         
    }


}