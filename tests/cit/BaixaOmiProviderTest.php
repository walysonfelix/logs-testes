<?php 

namespace Solides\Business\Logs\Tests;

use PHPUnit\Framework\TestCase;
use Solides\Business\Logs\Providers\cit\BaixaOmiProvider;


class BaixaOmiProviderTest extends TestCase {

    public function testSetStatusBaixa()
    {   
        $baixa = new BaixaOmiProvider();
        $expected = 'String teste';
        $baixa->setStatusBaixa($expected);  
        $actual = $baixa->getStatusBaixa();

        self::assertIsString($actual);
        
    }

    public function testSetMessage()
    {   
        $baixa = new BaixaOmiProvider();
        $expected = 'String_teste';
        $baixa->setMessage($expected);  
        $actual = $baixa->getMessage();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);        
    }

    public function testSetIdClifor()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 182;
        $baixa->setIdClifor($expected);  
        $actual = $baixa->getIdClifor();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetCliente()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 'Cliente Teste';
        $baixa->setCliente($expected);  
        $actual = $baixa->getCliente();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetIdMovimento()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 345;
        $baixa->setIdMovimento($expected);  
        $actual = $baixa->getIdMovimento();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetIdOs()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 171;
        $baixa->setIdOs($expected);  
        $actual = $baixa->getIdOs();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetValor()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 171.90;
        $baixa->setValor($expected);  
        $actual = $baixa->getValor();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }


    public function testSetDataVencimentoOmie()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = '2019-11-28 19:07:27';
        $baixa->setDataVencimentoOmie($expected);  
        $actual = $baixa->getDataVencimentoOmie();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }


    public function testSetNotaFiscal()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "33160702620499000165650030000026291000026299";
        $baixa->setNotaFiscal($expected);  
        $actual = $baixa->getNotaFiscal();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetDocumento()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "Documento text";
        $baixa->setDocumento($expected);  
        $actual = $baixa->getDocumento();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetDataPagamento()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "2019-11-28 19:07:27";
        $baixa->setDataPagamento($expected);  
        $actual = $baixa->getDataPagamento();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetPayloadOmie()
    {   
        $baixa = new BaixaOmiProvider();
        $expected = [
            'payload' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }'
        ];
        $baixa->setPayloadOmie($expected);  
        $actual = $baixa->getPayloadOmie();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetCreatedAt()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "2019-11-28 19:07:27";
        $baixa->setCreatedAt($expected);  
        $actual = $baixa->getCreatedAt();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetUpdatedAt()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "2019-11-28 19:07:27";
        $baixa->setUpdatedAt($expected);  
        $actual = $baixa->getUpdatedAt();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }


    public function testGetStatusBaixa()
    {   
        $baixa = new BaixaOmiProvider();
        $expected = 'String teste';
        $baixa->setStatusBaixa('cancelamentos');  
        $actual = $baixa->getStatusBaixa();

        self::assertIsString($actual);
        
    }

    public function testGetMessage()
    {   
        $baixa = new BaixaOmiProvider();
        $expected = 'String_teste';
        $baixa->setMessage('String_teste');  
        $actual = $baixa->getMessage();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);        
    }

    public function testGetIdClifor()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 182;
        $baixa->setIdClifor(182);  
        $actual = $baixa->getIdClifor();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetCliente()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 'Cliente Teste';
        $baixa->setCliente('Cliente Teste');  
        $actual = $baixa->getCliente();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetIdMovimento()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 345;
        $baixa->setIdMovimento(345);  
        $actual = $baixa->getIdMovimento();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetIdOs()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 171;
        $baixa->setIdOs(171);  
        $actual = $baixa->getIdOs();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetValor()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = 171.90;
        $baixa->setValor(171.90);  
        $actual = $baixa->getValor();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }


    public function testGetDataVencimentoOmie()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = '2019-11-28 19:07:27';
        $baixa->setDataVencimentoOmie('2019-11-28 19:07:27');  
        $actual = $baixa->getDataVencimentoOmie();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }


    public function testGetNotaFiscal()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "33160702620499000165650030000026291000026299";
        $baixa->setNotaFiscal("33160702620499000165650030000026291000026299");  
        $actual = $baixa->getNotaFiscal();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetDocumento()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "Documento text";
        $baixa->setDocumento("Documento text");  
        $actual = $baixa->getDocumento();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetDataPagamento()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "2019-11-28 19:07:27";
        $baixa->setDataPagamento("2019-11-28 19:07:27");  
        $actual = $baixa->getDataPagamento();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetPayloadOmie()
    {   
        $baixa = new BaixaOmiProvider();
        $expected = [
            'payload' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }'
        ];
        $baixa->setPayloadOmie([
            'payload' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }'
        ]);  
        $actual = $baixa->getPayloadOmie();

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetCreatedAt()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "2019-11-28 19:07:27";
        $baixa->setCreatedAt("2019-11-28 19:07:27");  
        $actual = $baixa->getCreatedAt();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetUpdatedAt()
    {   

        $baixa = new BaixaOmiProvider();
        $expected = "2019-11-28 19:07:27";
        $baixa->setUpdatedAt("2019-11-28 19:07:27");  
        $actual = $baixa->getUpdatedAt();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }
    
    public function testSetLogsPush()
    {
        $baixa = new BaixaOmiProvider();
        $expected = [
            'status_baixa' => 'Cancelado', 
            'message' => 'Seu acesso a plataforma da Sólides já está ativo, veja seus dados de acesso abaixo:', 
            'idclifor' => 172,  
            'cliente' => 'Cliente testes de oliveira',  
            'id_movimento' => 185, 
            'id_os' => 223, 
            'valor' => 172.90,  
            'data_vencimento_omie' => '2019-11-28 19:07:27', 
            'nota_fiscal' => '33160702620499000165650030000026291000026299', 
            'documento' => 'Documento text', 
            'data_pagamento' => '2019-11-28 19:07:27', 
            'payload_omie' => '{
                "Cabecalho": {
                    "cCodIntOS": "172#avulso#C#1649681533",
                    "cEtapa": "30",
                    "dDtPrevisao": "12\/04\/2022",
                    "nCodCli": 2372390866,
                    "nQtdeParc": 1,
                    "cCodParc": "000"
                }', 
            'created_at' => '2019-11-28 19:07:27', 
            'updated_at' => '2019-11-28 19:07:27', 
        ];
        $baixa->setPostBaixa($expected);   
        $actual = $expected;

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual); 
    }
}