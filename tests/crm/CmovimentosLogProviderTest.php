<?php 

namespace Solides\Business\Logs\Tests;

use PHPUnit\Framework\TestCase;
use Solides\Business\Logs\Providers\crm\CmovimentosLogProvider;


class CmovimentosLogProviderTest extends TestCase {

    public function testSetIDEMP()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 182;
        $movimento->setIDEMP($expected);  
        $actual = $movimento->getIDEMP();

        self::assertIsInt($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetIDCLIFOR()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 190;
        $movimento->setIDCLIFOR($expected);  
        $actual = $movimento->getIDCLIFOR();

        self::assertIsInt($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetIDMOVIMENTO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 200;
        $movimento->setIDMOVIMENTO($expected);  
        $actual = $movimento->getIDMOVIMENTO();

        self::assertIsInt($actual);
        self::assertEquals($expected, $actual);
    }


    public function testSetACAO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 'Pendente';
        $movimento->setACAO($expected);  
        $actual = $movimento->getACAO();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetMOTIVO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 'Cancelamento';
        $movimento->setMOTIVO($expected);  
        $actual = $movimento->getMOTIVO();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetEMAILUSUARIO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 'teste.solides@gmail.com';
        $movimento->setEMAILUSUARIO($expected);  
        $actual = $movimento->getEMAILUSUARIO();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetVALORANTIGO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 123.90;
        $movimento->setVALORANTIGO($expected);  
        $actual = $movimento->getVALORANTIGO();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetVALORNOVO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 140.90;
        $movimento->setVALORNOVO($expected);  
        $actual = $movimento->getVALORNOVO();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetDATAANTIGA()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = '2022-04-11 09:52:12';
        $movimento->setDATAANTIGA($expected);  
        $actual = $movimento->getDATAANTIGA();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetDATANOVA()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = '2022-04-11 09:52:12';
        $movimento->setDATANOVA($expected);  
        $actual = $movimento->getDATANOVA();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetDATAMODIFICACAO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = '2022-04-11 09:52:12';
        $movimento->setDATAMODIFICACAO($expected);  
        $actual = $movimento->getDATAMODIFICACAO();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetIDEMP()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 182;
        $movimento->setIDEMP(182);  
        $actual = $movimento->getIDEMP();

        self::assertIsInt($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetIDCLIFOR()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 190;
        $movimento->setIDCLIFOR(190);  
        $actual = $movimento->getIDCLIFOR();

        self::assertIsInt($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetIDMOVIMENTO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 200;
        $movimento->setIDMOVIMENTO(200);  
        $actual = $movimento->getIDMOVIMENTO();

        self::assertIsInt($actual);
        self::assertEquals($expected, $actual);
    }


    public function testGetACAO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 'Pendente';
        $movimento->setACAO('Pendente');  
        $actual = $movimento->getACAO();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetMOTIVO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 'Cancelamento';
        $movimento->setMOTIVO('Cancelamento');  
        $actual = $movimento->getMOTIVO();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetEMAILUSUARIO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 'teste.solides@gmail.com';
        $movimento->setEMAILUSUARIO('teste.solides@gmail.com');  
        $actual = $movimento->getEMAILUSUARIO();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetVALORANTIGO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 123.90;
        $movimento->setVALORANTIGO(123.90);  
        $actual = $movimento->getVALORANTIGO();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetVALORNOVO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = 140.90;
        $movimento->setVALORNOVO(140.90);  
        $actual = $movimento->getVALORNOVO();

        self::assertIsNumeric($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetDATAANTIGA()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = '2022-04-11 09:52:12';
        $movimento->setDATAANTIGA('2022-04-11 09:52:12');  
        $actual = $movimento->getDATAANTIGA();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetDATANOVA()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = '2022-04-11 09:52:12';
        $movimento->setDATANOVA('2022-04-11 09:52:12');  
        $actual = $movimento->getDATANOVA();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testGetDATAMODIFICACAO()
    {   

        $movimento = new CmovimentosLogProvider();
        $expected = '2022-04-11 09:52:12';
        $movimento->setDATAMODIFICACAO('2022-04-11 09:52:12');  
        $actual = $movimento->getDATAMODIFICACAO();

        self::assertIsString($actual);
        self::assertEquals($expected, $actual);
    }

    public function testSetPostCmovimentos()
    {
        $movimento = new CmovimentosLogProvider();
        $expected = [
            'IDEMP' => 182, 
            'IDCLIFOR' => 190, 
            'IDMOVIMENTO' => 222,  
            'ACAO' => 'Cancelamento',  
            'MOTIVO' => 'Pendencia', 
            'EMAILUSUARIO' => 'teste.solides@gmail.com.br', 
            'VALORANTIGO' => 123,45,  
            'VALORNOVO' => 150.56, 
            'DATAANTIGA' => '2022-04-11 09:52:12', 
            'DATANOVA' => '2022-04-11 09:52:12', 
            'DATAMODIFICACAO' => '2022-04-11 09:52:12',
        ];
        $movimento->setPostCmovimentos($expected);   
        $actual = $expected;

        self::assertIsArray($actual);
        self::assertEquals($expected, $actual); 
    }
    
}