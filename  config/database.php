<?php

return [
    'cit' => [
        'driver' => 'mysql',
        'url' => env('DATABASE_CIT_URL'),
        'host' => env('DB_CIT_HOST', 'localhost:3306'),
        'port' => env('DB_CIT_PORT', '3306'),
        'database' => env('DB_CIT_DATABASE', 'cit'),
        'username' => env('DB_CIT_USERNAME', 'root'),
        'password' => env('DB_CIT_PASSWORD', 'Solides'),
        'unix_socket' => env('DB_CIT_SOCKET', ''),
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'prefix_indexes' => true,
        'strict' => true,
        'engine' => null,
        'options' => extension_loaded('pdo_mysql') ? array_filter([
            PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
        ]) : [],
    ],

    'crm' => [
        'driver' => 'mysql',
        'url' => env('DATABASE_CRM_URL'),
        'host' => env('DB_CRM_HOST', '127.0.0.1'),
        'port' => env('DB_CRM_PORT', '3306'),
        'database' => env('DB_CRM_DATABASE', 'forge'),
        'username' => env('DB_CRM_USERNAME', 'forge'),
        'password' => env('DB_CRM_PASSWORD', ''),
        'unix_socket' => env('DB_CRM_SOCKET', ''),
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'prefix_indexes' => true,
        'strict' => false,
        'engine' => null,
        'options' => extension_loaded('pdo_mysql') ? array_filter([
            PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
        ]) : [],
    ],

    'partners' => [
        'driver' => 'mysql',
        'url' => env('DATABASE_PARTNERS_URL'),
        'host' => env('DB_PARTNERS_HOST', '127.0.0.1'),
        'port' => env('DB_PARTNERS_PORT', '3306'),
        'database' => env('DB_PARTNERS_DATABASE', 'forge'),
        'username' => env('DB_PARTNERS_USERNAME', 'forge'),
        'password' => env('DB_PARTNERS_PASSWORD', ''),
        'unix_socket' => env('DB_PARTNERS_SOCKET', ''),
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'prefix_indexes' => true,
        'strict' => true,
        'engine' => null,
        'options' => extension_loaded('pdo_mysql') ? array_filter([
            PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
        ]) : [],
    ],
];
