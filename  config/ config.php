<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Generator Config
    |--------------------------------------------------------------------------
    |
    */
    'generator' => [
        'rootNamespace' => 'App\\',
        'eloquentNamespace' => 'App\\Repositories\\Eloquent',
        'interfaceNamespace' => 'App\\Repositories\\Interface',
        'paths' => [
            'repositories' => 'Repositories/Eloquent/',
            'interfaces' => 'Repositories/Interface/',
        ],
        'databases' => [1 => 'CIT', 2 => 'CRM', 3 => 'Partners'],
    ]
];
