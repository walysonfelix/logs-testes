![Logo](https://solides.com.br/wp-content/uploads/2023/03/logo-solides-mockup-planejador-analista-comunicador-e-executor-lp-23.png)

# Solides Business Package logs

Projeto destinado a um pacote para que esse seja importado em novos e antigos projetos, atendendo a demandas futuras trazendo consistencia ao fluxo, maior abrangencia quanto a novas solicitações vindas de diversos lugares, maior manutenabilidade e controle

### Pré Requisitos

- PHP 8.1

### Instalação

1. Entre no seu projeto
2. Abra o composer.json
3. Coloque a referência do package dentro da tag repositories (caso não exista crie como no exemplo)
   ```json
   "repositories": [
         {
            "type": "vcs",
            "url": "https://gitlab.com/solides/business/core/packages/logs"
         }
   ]
   ```
4. Caso tenha um servidor satis referencie como no exemplo a seguir:
   ```json
   "repositories": [
         {
            "type": "composer",
            "url": "https://gitlab.com/solides/business/core/packages/logs.git"
         }
   ]
   ```
5. Aplique as novas configurações usando o comando composer required
```bash
$ composer required packages/logs
```
### Features



# Utilização


# License
MIT