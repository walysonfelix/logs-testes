# Changelog
Todas as alterações notáveis neste projeto serão documentadas neste arquivo.

O formato é baseado no [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
e este projeto segue [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [1.0.0] - 14/06/2023

### Added
Custom Exception:
- JwtServiceException

Funções:
- addCitLog       [CIT] Função que insere os dados na tabela de cit_log do banco CIT
- addBaixa        [CIT] Função que insere os dados na tabela de log_baixa_omie do banco CIT
- addAutomacao    [CIT] Função que insere os dados na tabela de citlog_automacoes do banco CIT
- addCmovimentos  [CRM] Função que insere os dados na tabela de CMOVIMENTOSLOG do banco CRM

### Added
- Lançamento inicial do pacote.