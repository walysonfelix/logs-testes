<?php
namespace Solides\Business\Logs\Providers\cit;

use Parsedown;
use Illuminate\Support\Carbon;
use Carbon\Exceptions\Exception;
use Illuminate\Support\Facades\Request;
use Solides\Business\Logs\Models\cit\CitLog;
use Solides\Business\Logs\Exceptions\LogsServiceException;

/**
 * Summary of TraitTeste
 */
class CitLogProvider
{

    

    /**
     * @var string
     */

    public string $acao = '';

    /**
     * @var string
     */
    private string $infoMsg = '';

    /**
     * @var string
     */
    private string $setor = '';

    /**
     * @var string
     */
    private string $request_from = '';

    /**
     * @var integer
     */
    private ?int $idclifor;

    /**
     * @var integer
     */
    private ?int $dealId;

    /**
     * @var integer
     */
    private ?int $idGestao;

    /**
     * @var string
     */
    private string $status = '';

    /**
     * @var integer
     */
    private ?int $hubspotOwnerId;

    /**
     * @var string
     */
    private string $disparadoPor = '';

    /**
     * @var array
     */
    private array $curlInfo = [];

    /**
     * @var array
     */
    private array $payload = [];

    /**
     * @var array
     */
    private array $response = [];

    /**
     * Summary of createdAt
     * @var 
     */
    private string $createdAt = '';

    /**
     * @var string
     */
    private string $httpCode = '';

    /**
     * @var array
     */
    private array $logs = [];



    public function setAcao(string $acao): self
    {

        if (is_numeric($acao)) {
            throw new LogsServiceException('O Parâmetro [acao] não pode conter um valor númerico!');
        }

        if (is_int($acao)) {
            throw new LogsServiceException('O Parâmetro [acao] não pode conter um valor de um numero inteiro!');
        }

        return $this;
    }

    public function setInfoMsg(string $infoMsg): self
    {

        if (is_numeric($infoMsg)) {
            throw new LogsServiceException('O Parâmetro [infoMsg] não pode conter um valor númerico!');
        }

        if (is_int($infoMsg)) {
            throw new LogsServiceException('O Parâmetro [infoMsg] não pode conter um valor de um numero inteiro!');
        }
        
        $this->infoMsg = $infoMsg;
        return $this;
    }

    public function setSetor(string $setor): self
    {

        if (is_numeric($setor)) {
            throw new LogsServiceException('O Parâmetro [setor] não pode conter um valor númerico!');
        }

        if (is_int($setor)) {
            throw new LogsServiceException('O Parâmetro [setor] não pode conter um valor de um numero inteiro!');
        }
        
  
        $this->setor = $setor;
        return $this;
    }

    public function setRequestFrom(string $request_from): self
    {

        if (is_numeric($request_from)) {
            throw new LogsServiceException('O Parâmetro [request_from] não pode conter um valor númerico!');
        }

        if (is_int($request_from)) {
            throw new LogsServiceException('O Parâmetro [request_from] não pode conter um valor de um numero inteiro!');
        }
  
        $this->request_from = $request_from;
        return $this;

    }

    public function setIdClifor(int $idclifor): self
    {

        if (is_string($idclifor)) {
            throw new LogsServiceException('O Parâmetro [idclifor] não pode conter um valor de tipo texto!');
        }
  
        $this->idclifor = $idclifor;
        return $this;
    }

    public function setDealId(int $dealId): self
    {
        if (is_string($dealId)) {
            throw new LogsServiceException('O Parâmetro [dealId] não pode conter um valor de tipo texto!');
        }
        $this->dealId = $dealId;
        return $this;
    }


    public function setIdGestao(int $idGestao):self
    {
        if (is_string($idGestao)) {
            throw new LogsServiceException('O Parâmetro [idGestao] não pode conter um valor de tipo texto!');
        }

        $this->idGestao = $idGestao;
        return $this;
    }

    public function setStatus(string $status):self
    {

        if (is_numeric($status)) {
            throw new LogsServiceException('O Parâmetro [status] não pode conter um valor númerico!');
        }

        $this->status = $status;
        return $this;
    }

    public function setHubspotOwnerId(int $hubspotOwnerId):self
    {

        if (is_string($hubspotOwnerId)) {
            throw new LogsServiceException('O Parâmetro [hubspotOwnerId] não pode conter um valor de tipo texto!');
        }

        $this->hubspotOwnerId = $hubspotOwnerId;
        return $this;
    }

    public function setDisparadoPor(string $disparadoPor):self
    {
        if (is_numeric($disparadoPor)) {
            throw new LogsServiceException('O Parâmetro [disparadoPor] não pode conter um valor númerico!');
        }

        $this->disparadoPor = $disparadoPor;
        return $this;
    }

    public function setCurlInfo(array $curlInfo):self
    {   

        if (!is_array($curlInfo)) {
            throw new LogsServiceException('O Parâmetro [curlInfo] tem que conter um array!');
        }

        if (is_numeric($curlInfo)) {
            throw new LogsServiceException('O Parâmetro [curlInfo] não pode conter um valor númerico!');
        }

        $this->curlInfo = $curlInfo;
        return $this;
    }

    public function setPayload(array $payload):self
    {   
        if (!is_array($payload)) {
            throw new LogsServiceException('O Parâmetro [payload] tem que conter um array!');
        }

        if (is_numeric($payload)) {
            throw new LogsServiceException('O Parâmetro [payload] não pode conter um valor númerico!');
        }
        $this->payload = $payload;
        return $this;
    }
    
    public function setResponse(array $response):self
    {   

        if (!is_array($response)) {
            throw new LogsServiceException('O Parâmetro [response] tem que conter um array!');
        }

        if (is_numeric($response)) {
            throw new LogsServiceException('O Parâmetro [response] não pode conter um valor númerico!');
        }

        $this->response = $response;
        return $this;
    }

    public function setCreatedAt(string $createdAt):self
    {   

        if (is_numeric($createdAt)) {
            throw new LogsServiceException('O Parâmetro [createdAt] não pode conter um valor númerico!');
        }

        $this->createdAt = $createdAt;
        return $this;
    }

    public function setHttpCode(string $httpCode):self
    {   

        if (!is_string($httpCode)) {
            throw new LogsServiceException('O Parâmetro [http_code] deve ser do tipo [http_code] e não deve ser nulo.');
        }
        
        $this->httpCode = $httpCode;
        return $this;
    }

    public function getAcao():string
    {
        return $this->acao;
    }
    public function getInfoMsg(): string
    {
        return $this->infoMsg;
    }

    public function getSetor(): string
    {
        return $this->setor;
    }

    public function getRequestFrom(): string
    {
        return $this->request_from;
    }

    public function getIdClifor(): string
    {
        return $this->idclifor;
    } 

    public function getHttpCode():string
    {
        return $this->httpCode;
    }

    public function getDealId():int
    {
        return $this->dealId;
    }

    public function getIdGestao():int
    {
        return $this->idGestao;
    }

    public function getStatus():string
    {
        return $this->status;
    }

    public function getHubspotOwnerId():int
    {
        return $this->hubspotOwnerId;
    }

    public function getDisparadoPor():string
    {
        return $this->disparadoPor;
    }

    public function getCurlInfo():array
    {
        return $this->curlInfo;
    }
    
    public function getPayload():array
    {
        return $this->payload;
    }

    public function getResponse():array
    {
        return $this->response;
    }


    public function getCreatedAt():string
    {
        return $this->createdAt;
    }

    
    public function setLogsPush(array $logs):self
    {
        if (empty($logs)) {
            throw new LogsServiceException('O Parâmetro [logs] não pode conter um valor vazio!');
        }

        if (!is_array($logs)) {
            throw new LogsServiceException('O Parâmetro [logs] tem que conter um array!');
        }

        if (is_numeric($logs)) {
            throw new LogsServiceException('O Parâmetro [logs] não pode conter um valor númerico!');
        }

        if (is_string($logs)) {
            throw new LogsServiceException('O Parâmetro [idclifor] não pode conter um valor de tipo texto!');
        }

        try {

            $this->logs = $logs;
            return $this;
            // return $this->addCitLog($logs);
        
        }catch (\Throwable $th) {

            throw new LogsServiceException("Exceção capturada:".$th->getMessage()." Error");

        }

    
    }

    public function addCitLog(array $logs)
    {   

        if (empty($logs)) {
            throw new LogsServiceException('O Parâmetro [logs] não pode conter um valor vazio!');
        }

        if (!is_array($logs)) {
            throw new LogsServiceException('O Parâmetro [logs] tem que conter um array!');
        }

        if (is_numeric($logs)) {
            throw new LogsServiceException('O Parâmetro [logs] não pode conter um valor númerico!');
        }

        if (is_string($logs)) {
            throw new LogsServiceException('O Parâmetro [idclifor] não pode conter um valor de tipo texto!');
        }


        try {
            
            $log = CitLog::create([
                'acao' => $logs['acao'] ?? null,
                'info_msg' => $logs['info_msg'] ?? null,
                'setor' => $logs['setor'] ?? null,
                'request_from' => $logs['request_from'] ?? null,
                'logs_from' => $logs['logs_from'] ?? null,
                'idclifor' => $logs['idclifor'] ?? null,
                'deal_id' => $logs['deal_id'] ?? null,
                'id_gestao' => $logs['id_gestao'] ?? null,
                'status' => $logs['status'] ?? null,
                'hubspot_owner_id' => $logs['hubspot_owner_id'] ?? null,
                'disparadoPor' => $logs['disparadoPor'] ?? null,
                'curl_info' => $logs['curl_info'] ?? null,
                'payload' => json_encode($logs['payload']) ?? null,
                'response' => json_encode($logs['response']) ?? null,
                'http_code' => $logs['httpCode'] ?? null,
                'createdAt' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
            $log->save();

            throw new LogsServiceException("Sucesso!");

        } catch (\Throwable $th) {

            throw new LogsServiceException("Exceção capturada: ".$th->getMessage()."\n");

        }

        
    }
 


}