<?php
namespace Solides\Business\Logs\Providers\cit;

use Parsedown;
use Illuminate\Support\Carbon;
use Carbon\Exceptions\Exception;
use Illuminate\Support\Facades\Request;
use Solides\Business\Logs\Models\cit\CitAutomacao;
use Solides\Business\Logs\Exceptions\LogsServiceException;

/**
 * Summary of TraitTeste
 */
class AutomecaoProvider
{

    /**
     * @var string
     */

     private string $acao = '';

     /**
     * @var string
     */
    private string $status = '';

    /**
     * @var string
     */
    private string $infoMsg = '';

    /**
     * @var string
     */
    private string $setor = '';

    /**
     * @var string
     */
    private string $request_from = '';

    /**
     * @var integer
     */
    private ?int $target = null;

    /**
     * @var integer
     */
    private ?int $idclifor;

    /**
     * @var integer
     */
    private ?int $dealId;


    /**
     * @var integer
     */
    private ?int $companyId;


    /**
     * @var integer
     */
    private int $idGestao;

    /**
     * @var integer
     */
    private ?int $hubspotOwnerId;

    /**
     * @var array
     */
    private array $curlInfo = [];


    /**
     * @var string
     */
    private string $httpCode = '';

    /**
     * @var array
     */
    private array $payload = [];


    /**
     * @var array
     */
    private array $response = [];


    /**
     * @var string
     */
    private string $disparadoPor = '';


    /**
     * @var integer
     */
    private ?int $messageId;

    /**
     * @var integer
     */
    private ?int $attempts;


     /**
     * Summary of createdAt
     * @var 
     */
    private string $createdAt = '';

    /**
     * @var array
     */
    private array $auto = [];

    /**
     * Summary of setAcao
     * @param mixed $acao
     * @throws \Solides\Business\Logs\Exceptions\LogsServiceException
     * @return \Solides\Business\Logs\Providers\cit\AutomecaoProvider
     */
    public function setAcao(string $acao): self
    {

        if (is_numeric($acao)) {
            throw new LogsServiceException('O Parâmetro [acao] não pode conter um valor númerico!');
        }

        return $this;
    }


    /**
     * Summary of getAcao
     * @return string
     */
    public function getAcao():string
    {
        return $this->acao;
    }


    public function setStatus(string $status):self
    {

        if (is_numeric($status)) {
            throw new LogsServiceException('O Parâmetro [status] não pode conter um valor númerico!');
        }

        $this->status = $status;
        return $this;
    }


    public function getStatus():string
    {
        return $this->status;
    }

    public function setInfoMsg(string $infoMsg): self
    {

        if (is_numeric($infoMsg)) {
            throw new LogsServiceException('O Parâmetro [infoMsg] não pode conter um valor númerico!');
        }
        
        $this->infoMsg = $infoMsg;
        return $this;
    }

    public function getInfoMsg(): string
    {
        return $this->infoMsg;
    }

    public function setSetor(string $setor): self
    {

        if (is_numeric($setor)) {
            throw new LogsServiceException('O Parâmetro [setor] não pode conter um valor númerico!');
        }
  
        $this->setor = $setor;
        return $this;
    }

    public function getSetor(): string
    {
        return $this->setor;
    }

    public function setRequestFrom(string $request_from): self
    {

        if (is_numeric($request_from)) {
            throw new LogsServiceException('O Parâmetro [request_from] não pode conter um valor númerico!');
        }
  
        $this->request_from = $request_from;
        return $this;

    }

    public function getRequestFrom(): string
    {
        return $this->request_from;
    }


    /**
     * Summary of setTarget
     * @param mixed $target
     * @throws \Solides\Business\Logs\Exceptions\LogsServiceException
     * @return \Solides\Business\Logs\Providers\cit\AutomecaoProvider
     */
    public function setTarget(?int $target): self {
		$this->target = $target;
		return $this;
	}

    /**
     * Summary of getTarget
     * @return int
     */
    public function getTarget(): ?int 
    {
		return $this->target;
	}

    public function setIdClifor(int $idclifor): self
    {

        if (is_string($idclifor)) {
            throw new LogsServiceException('O Parâmetro [idclifor] não pode conter um valor de tipo texto!');
        }
  
        $this->idclifor = $idclifor;
        return $this;
    }


    public function getIdClifor(): string
    {
        return $this->idclifor;
    } 
    

    public function setDealId(int $dealId): self
    {
        if (is_string($dealId)) {
            throw new LogsServiceException('O Parâmetro [dealId] não pode conter um valor de tipo texto!');
        }
        $this->dealId = $dealId;
        return $this;
    }

    public function getDealId():int
    {
        return $this->dealId;
    }

    /**
	 * 
	 * @param  $companyId 
	 * @return self
	 */
	public function setCompanyId(?int $companyId): self {
		$this->companyId = $companyId;
		return $this;
	}

	/**
	 * 
	 * @return 
	 */
	public function getCompanyId(): ?int {
		return $this->companyId;
	}

    public function setIdGestao(int $idGestao):self
    {
        if (is_string($idGestao)) {
            throw new LogsServiceException('O Parâmetro [idGestao] não pode conter um valor de tipo texto!');
        }

        $this->idGestao = $idGestao;
        return $this;
    }

    public function getIdGestao():int
    {
        return $this->idGestao;
    }

    public function setHubspotOwnerId(int $hubspotOwnerId):self
    {

        if (is_string($hubspotOwnerId)) {
            throw new LogsServiceException('O Parâmetro [hubspotOwnerId] não pode conter um valor de tipo texto!');
        }

        $this->hubspotOwnerId = $hubspotOwnerId;
        return $this;
    }


    public function getHubspotOwnerId():int
    {
        return $this->hubspotOwnerId;
    }

    public function setCurlInfo(array $curlInfo):self
    {   

        if (!is_array($curlInfo)) {
            throw new LogsServiceException('O Parâmetro [curlInfo] tem que conter um array!');
        }

        if (is_numeric($curlInfo)) {
            throw new LogsServiceException('O Parâmetro [curlInfo] não pode conter um valor númerico!');
        }

        $this->curlInfo = $curlInfo;
        return $this;
    }



    public function getCurlInfo():array
    {
        return $this->curlInfo;
    }

    public function setHttpCode(string $httpCode):self
    {   

        if (!is_string($httpCode)) {
            throw new LogsServiceException('O Parâmetro [http_code] deve ser do tipo [http_code] e não deve ser nulo.');
        }
        
        $this->httpCode = $httpCode;
        return $this;
    }

    public function getHttpCode():string
    {
        return $this->httpCode;
    }

    public function setPayload(array $payload):self
    {   
        if (!is_array($payload)) {
            throw new LogsServiceException('O Parâmetro [payload] tem que conter um array!');
        }

        if (is_numeric($payload)) {
            throw new LogsServiceException('O Parâmetro [payload] não pode conter um valor númerico!');
        }
        $this->payload = $payload;
        return $this;
    }

    public function getPayload():array
    {
        return $this->payload;
    }

    public function setResponse(array $response):self
    {   

        if (!is_array($response)) {
            throw new LogsServiceException('O Parâmetro [response] tem que conter um array!');
        }

        if (is_numeric($response)) {
            throw new LogsServiceException('O Parâmetro [response] não pode conter um valor númerico!');
        }

        $this->response = $response;
        return $this;
    }

    public function getResponse():array
    {
        return $this->response;
    }

    public function setDisparadoPor(string $disparadoPor):self
    {
        if (is_numeric($disparadoPor)) {
            throw new LogsServiceException('O Parâmetro [disparadoPor] não pode conter um valor númerico!');
        }

        $this->disparadoPor = $disparadoPor;
        return $this;
    }

    public function getDisparadoPor():string
    {
        return $this->disparadoPor;
    }

    /**
     * Summary of setMessageId
     * @param mixed $messageId
     */
    public function setMessageId(int $messageId): self
    {
        if (is_string($messageId)) {
            throw new LogsServiceException('O Parâmetro [dealId] não pode conter um valor de tipo texto!');
        }
        $this->messageId = $messageId;
        return $this;
    }

    /**
     * Summary of getMessageId
     * @return int
     */
    public function getMessageId():int
    {
        return $this->messageId;
    }

    public function setTattempts(int $attempts): self
    {
        if (is_string($attempts)) {
            throw new LogsServiceException('O Parâmetro [dealId] não pode conter um valor de tipo texto!');
        }
        $this->attempts = $attempts;
        return $this;
    }

    /**
     * Summary of geTattempts
     * @return int
     */
    public function geTattempts():int
    {
        return $this->attempts;
    }

    public function setCreatedAt(string $createdAt):self
    {   

        if (is_numeric($createdAt)) {
            throw new LogsServiceException('O Parâmetro [createdAt] não pode conter um valor númerico!');
        }

        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt():string
    {
        return $this->createdAt;
    }

    public function setPostAutomocao(array $auto):self
    {

        if (empty($auto)) {
            throw new LogsServiceException('O Parâmetro [auto] não pode conter um valor vazio!');
        }

        if (!is_array($auto)) {
            throw new LogsServiceException('O Parâmetro [auto] tem que conter um array!');
        }

        if (is_numeric($auto)) {
            throw new LogsServiceException('O Parâmetro [auto] não pode conter um valor númerico!');
        }

        if (is_string($auto)) {
            throw new LogsServiceException('O Parâmetro [auto] não pode conter um valor de tipo texto!');
        }

        try {

            $this->auto = $auto;
            return $this;
        
        }catch (\Throwable $th) {

            throw new LogsServiceException("Exceção capturada:".$th->getMessage()."\n");

        }

    }

    public function addAutomacao(array $auto)
    {   

        if (empty($auto)) {
            throw new LogsServiceException('O Parâmetro [auto] não pode conter um valor vazio!');
        }

        if (!is_array($auto)) {
            throw new LogsServiceException('O Parâmetro [auto] tem que conter um array!');
        }

        if (is_numeric($auto)) {
            throw new LogsServiceException('O Parâmetro [auto] não pode conter um valor númerico!');
        }

        if (is_string($auto)) {
            throw new LogsServiceException('O Parâmetro [auto] não pode conter um valor de tipo texto!');
        }


        try {
            
            $log = CitAutomacao::create([
                'acao' => $auto['acao'] ?? null,
                'status' => $auto['status'] ?? null,
                'info_msg' => $auto['info_msg']?? null,
                'setor' => $auto['setor']?? null,
                'request_from' => $auto['request_from'] ?? null,
                'target' => $auto['target'] ?? null,
                'idclifor' => $auto['idclifor'] ?? null,
                'deal_id' => $auto['deal_id'] ?? null,
                'company_id' => $auto['company_id'] ?? null,
                'id_gestao' => $auto['id_gestao'] ?? null,
                'hubspot_owner_id' => $auto['hubspot_owner_id'] ?? null,
                'curl_info' => $auto['curl_info'] ?? null,
                'http_code' => $auto['httpCode'] ?? null,
                'payload' => json_encode($auto['payload']) ?? null,
                'response' => json_encode($auto['response']) ?? null,
                'disparadoPor' => $auto['disparadoPor'] ?? null,
                'message_id' => $auto['message_id'] ?? null,
                'attempts' => $auto['attempts'] ?? null,
                'createdAt' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
            $log->save();

            throw new LogsServiceException("Sucesso!");

        } catch (\Throwable $th) {

            throw new LogsServiceException("Exceção capturada: ".$th->getMessage()."\n");

        }

        
    }
}