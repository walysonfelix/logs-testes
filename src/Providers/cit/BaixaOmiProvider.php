<?php
namespace Solides\Business\Logs\Providers\cit;

use Parsedown;
use Illuminate\Support\Carbon;
use Carbon\Exceptions\Exception;
use Faker\Core\Number;
use Illuminate\Support\Facades\Request;
use Ramsey\Uuid\Type\Decimal;
use Solides\Business\Logs\Models\cit\CitBaixaOmi;
use Solides\Business\Logs\Exceptions\LogsServiceException;


class BaixaOmiProvider
{
    /**
     * @var string
     */
    private string $statusBaixa = '';

    /**
     * @var string
     */
    private string $message = '';

    /**
     * @var integer
     */
    private ?int $idclifor = null;

    /**
     * @var string
     */
    private string $cliente = '';

    /**
     * @var integer
     */
    private ?int $idMovimento = null;


    /**
     * @var integer
     */
    private ?int $idOs = null;

    /**
     * @var float
     */
    private ?float $valor = null;

    /**
     * @var string
     */
    private string $dataVencimentoOmie = '';

    /**
     * @var string
     */
    private string $notaFiscal = '';

    /**
     * @var string
     */
    private string $documento = '';

    /**
     * @var string
     */
    private string $dataPagamento = '';

    /**
     * @var array
     */
    private array $payloadOmie = [];

    /**
     * @var string
     */
    private string $createdAt = '';


    /**
     * @var string
     */
    private string $updatedAt = '';

    /**
     * @var array
     */
    private array $baixa = [];

    public function setStatusBaixa(string $statusBaixa): self
    {

        if (is_numeric($statusBaixa)) {
            throw new LogsServiceException('O Parâmetro [statusBaixa] não pode conter um valor númerico!');
        }

        return $this;
    }

    public function getStatusBaixa():string
    {
        return $this->statusBaixa;
    }

	/**
	 * 
	 * @return 
	 */
	public function getMessage(): string 
    {
		return $this->message;
	}
	
	/**
	 * 
	 * @param  $message 
	 * @return self
	 */
	public function setMessage(string $message): self 
    {

        if (is_numeric($message)) {
            throw new LogsServiceException('O Parâmetro [message] não pode conter um valor númerico!');
        }

		$this->message = $message;
		return $this;
	}

    /**
	 * 
	 * @param  $idclifor 
	 * @return self
	 */
	public function setIdclifor(?int $idclifor): self 
    {

        if (is_string($idclifor)) {
            throw new LogsServiceException('O Parâmetro [idclifor] não pode conter um valor de tipo texto!');
        }

		$this->idclifor = $idclifor;
		return $this;
	}


	/**
	 * 
	 * @return 
	 */
	public function getIdclifor(): ?int {
        
		return $this->idclifor;
	}
	
    /**
	 * 
	 * @param  $cliente 
	 * @return self
	 */
	public function setCliente(string $cliente): self 
    {

        if (is_numeric($cliente)) {
            throw new LogsServiceException('O Parâmetro [cliente] não pode conter um valor númerico!');
        }
        
		$this->cliente = $cliente;
		return $this;
	}
	

	/**
	 * 
	 * @return 
	 */
	public function getCliente(): string 
    {

		return $this->cliente;
	}
	
	/**
	 * 
	 * @param  $idMovimento 
	 * @return self
	 */
	public function setIdMovimento(?int $idMovimento): self 
    {

        if (is_string($idMovimento)) {
            throw new LogsServiceException('O Parâmetro [idMovimento] não pode conter um valor de tipo texto!');
        }
        
		$this->idMovimento = $idMovimento;
		return $this;
	}

	/**
	 * 
	 * @return 
	 */
	public function getIdMovimento(): ?int 
    {
		return $this->idMovimento;
	}
	
	/**
	 * 
	 * @param  $idOs 
	 * @return self
	 */
	public function setIdOs(?int $idOs): self 
    {
        if (is_string($idOs)) {
            throw new LogsServiceException('O Parâmetro [idOs] não pode conter um valor de tipo texto!');
        }
        
		$this->idOs = $idOs;
		return $this;
	}

	/**
	 * 
	 * @return 
	 */
	public function getIdOs(): ?int {
		return $this->idOs;
	}
	
	/**
	 * 
	 * @param  $valor 
	 * @return self
	 */
	public function setValor(?float $valor): self 
    {

        if (is_string($valor)) {
            throw new LogsServiceException('O Parâmetro [valor] não pode conter um valor de tipo texto!');
        }
        
		$this->valor = $valor;
		return $this;
	}

	/**
	 * 
	 * @return 
	 */
	public function getValor(): ?float 
    {
		return $this->valor;
	}
	
	/**
	 * 
	 * @param  $dataVencimentoOmie 
	 * @return self
	 */
	public function setDataVencimentoOmie(string $dataVencimentoOmie): self
    {
        if (is_numeric($dataVencimentoOmie)) {
            throw new LogsServiceException('O Parâmetro [dataVencimentoOmie] não pode conter um valor númerico!');
        }
        
		$this->dataVencimentoOmie = $dataVencimentoOmie;
		return $this;
	}

	/**
	 * 
	 * @return 
	 */
	public function getDataVencimentoOmie(): string 
    {
		return $this->dataVencimentoOmie;
	}
	
    /**
	 * 
	 * @param  $notaFiscal 
	 * @return self
	 */
	public function setNotaFiscal(string $notaFiscal): self 
    {
        if (is_int($notaFiscal)) {
            throw new LogsServiceException('O Parâmetro [notaFiscal] não pode conter um valor inteiro!');
        }
        
		$this->notaFiscal = $notaFiscal;
		return $this;
	}
	

	/**
	 * 
	 * @return 
	 */
	public function getNotaFiscal(): string 
    {
		return $this->notaFiscal;
	}
	
	
    /**
	 * 
	 * @param  $documento 
	 * @return self
	 */
	public function setDocumento(string $documento): self 
    {

        if (is_int($documento)) {
            throw new LogsServiceException('O Parâmetro [documento] não pode conter um valor inteiro!');
        }
        
		$this->documento = $documento;
		return $this;
	}
    
	/**
	 * 
	 * @return 
	 */
	public function getDocumento(): string {
		return $this->documento;
	}
	
	/**
	 * 
	 * @param  $dataPagamento 
	 * @return self
	 */
	public function setDataPagamento(string $dataPagamento): self {

        if (is_int($dataPagamento)) {
            throw new LogsServiceException('O Parâmetro [dataPagamento] não pode conter um valor inteiro!');
        }
        
		$this->dataPagamento = $dataPagamento;
		return $this;
	}


	/**
	 * 
	 * @return 
	 */
	public function getDataPagamento(): string {
		return $this->dataPagamento;
	}
	
	/**
	 * 
	 * @param  $payloadOmie 
	 * @return self
	 */
	public function setPayloadOmie(array $payloadOmie): self {

        if (!is_array($payloadOmie)) {
            throw new LogsServiceException('O Parâmetro [payloadOmie] tem que conter um array!');
        }

        if (is_numeric($payloadOmie)) {
            throw new LogsServiceException('O Parâmetro [payloadOmie] não pode conter um valor númerico!');
        }

		$this->payloadOmie = $payloadOmie;
		return $this;
	}

	/**
	 * 
	 * @return 
	 */
	public function getPayloadOmie(): array {
		return $this->payloadOmie;
	}
	
	/**
	 * 
	 * @param  $createdAt 
	 * @return self
	 */
	public function setCreatedAt(string $createdAt): self 
    {
        if (is_int($createdAt)) {
            throw new LogsServiceException('O Parâmetro [createdAt] não pode conter um valor inteiro!');
        }
    
        
		$this->createdAt = $createdAt;
		return $this;
	}

	/**
	 * 
	 * @return 
	 */
	public function getCreatedAt(): string {
		return $this->createdAt;
	}



    /**
	 * 
	 * @param  $updatedAt 
	 * @return self
	 */
	public function setUpdatedAt(string $updatedAt): self 
    {
        if (is_int($updatedAt)) {
            throw new LogsServiceException('O Parâmetro [updatedAt] não pode conter um valor inteiro!');
        }

		$this->updatedAt = $updatedAt;
		return $this;
	}
    

	/**
	 * 
	 * @return 
	 */
	public function getUpdatedAt(): string {
		return $this->updatedAt;
	}

    public function setPostBaixa(array $baixa):self
    {

        if (empty($baixa)) {
            throw new LogsServiceException('O Parâmetro [baixa] não pode conter um valor vazio!');
        }

        if (!is_array($baixa)) {
            throw new LogsServiceException('O Parâmetro [baixa] tem que conter um array!');
        }

        if (is_numeric($baixa)) {
            throw new LogsServiceException('O Parâmetro [baixa] não pode conter um valor númerico!');
        }

        if (is_string($baixa)) {
            throw new LogsServiceException('O Parâmetro [baixa] não pode conter um valor de tipo texto!');
        }

        try {

            $this->baixa = $baixa;
            return $this;
        
        }catch (\Throwable $th) {

            throw new LogsServiceException("Exceção capturada:".$th->getMessage()."\n");

        }

    }

    public function addBaixa(array $baixa)
    {   

        if (empty($baixa)) {
            throw new LogsServiceException('O Parâmetro [baixa] não pode conter um valor vazio!');
        }

        if (!is_array($baixa)) {
            throw new LogsServiceException('O Parâmetro [baixa] tem que conter um array!');
        }

        if (is_numeric($baixa)) {
            throw new LogsServiceException('O Parâmetro [baixa] não pode conter um valor númerico!');
        }

        if (is_string($baixa)) {
            throw new LogsServiceException('O Parâmetro [baixa] não pode conter um valor de tipo texto!');
        }


        try {
            
            $log = CitBaixaOmi::create([
                'status_baixa' => $baixa['status_baixa'] ?? null,
                'message' => $baixa['message'] ?? null,
                'idclifor' => $baixa['idclifor']?? null,
                'cliente' => $baixa['cliente']?? null,
                'id_movimento' => $baixa['id_movimento'] ?? null,
                'id_os' => $baixa['id_os'] ?? null,
                'valor' => $baixa['valor'] ?? null,
                'data_vencimento_omie' => $baixa['data_vencimento_omie'] ?? null,
                'nota_fiscal' => $baixa['nota_fiscal'] ?? null,
                'documento' => $baixa['documento'] ?? null,
                'data_pagamento' => $baixa['data_pagamento'] ?? null,
                'payload_omie' => $baixa['payload_omie'] ?? null,
                'createdAt' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
            $log->save();

            throw new LogsServiceException("Sucesso!");

        } catch (\Throwable $th) {

            throw new LogsServiceException("Exceção capturada: ".$th->getMessage()."\n");

        }

        
    }
	
	
}