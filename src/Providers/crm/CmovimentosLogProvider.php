<?php
namespace Solides\Business\Logs\Providers\crm;

use Parsedown;
use Illuminate\Support\Carbon;
use Carbon\Exceptions\Exception;
use Illuminate\Support\Facades\Request;
use Solides\Business\Logs\Models\crm\CmovimentosLog;
use Solides\Business\Logs\Exceptions\LogsServiceException;


class CmovimentosLogProvider
{


    /**
     * Summary of IDEMP
     * @var 
     */
    private ?int $IDEMP;

    /**
     * Summary of IDCLIFOR
     * @var 
     */
    private ?int $IDCLIFOR;
    
    /**
     * Summary of IDMOVIMENTO
     * @var 
     */
    private ?int $IDMOVIMENTO;

    /**
     * Summary of ACAO
     * @var 
     */
    private string $ACAO = '';

    /**
     * Summary of MOTIVO
     * @var 
     */
    private string $MOTIVO = '';

    /**
     * Summary of EMAILUSUARIO
     * @var 
     */
    private string $EMAILUSUARIO = '';

    /**
     * Summary of VALORANTIGO
     * @var 
     */
    private ?float $VALORANTIGO;

    /**
     * Summary of VALORNOVO
     * @var 
     */
    private ?float $VALORNOVO;
    
    /**
     * Summary of DATAANTIGA
     * @var 
     */
    private string $DATAANTIGA;
    
    /**
     * Summary of DATANOVA
     * @var 
     */
    private string $DATANOVA;

    /**
     * Summary of DATAMODIFICACAO
     * @var 
     */
    private string $DATAMODIFICACAO;
    
    /**
     * Summary of DATAMODIFICACAO
     * @var 
     */
    private array $movimento = [];



    /**
	 * Summary of IDEMP
	 * @param  $IDEMP Summary of IDEMP
	 * @return self
	 */
	public function setIDEMP(?int $IDEMP): self 
    {
        if (is_string($IDEMP)) {
            throw new LogsServiceException('O Parâmetro [IDEMP] não pode conter um valor de tipo texto!');
        }
        
		$this->IDEMP = $IDEMP;
		return $this;
	}

	/**
	 * Summary of IDEMP
	 * @return 
	 */
	public function getIDEMP(): ?int 
    {
		return $this->IDEMP;
	}
	
	
    /**
	 * Summary of IDCLIFOR
	 * @param  $IDCLIFOR Summary of IDCLIFOR
	 * @return self
	 */
	public function setIDCLIFOR(?int $IDCLIFOR): self 
    {
        if (is_string($IDCLIFOR)) {
            throw new LogsServiceException('O Parâmetro [IDCLIFOR] não pode conter um valor de tipo texto!');
        }
        
		$this->IDCLIFOR = $IDCLIFOR;
		return $this;
	}

	/**
	 * Summary of IDCLIFOR
	 * @return 
	 */
	public function getIDCLIFOR(): ?int 
    {
		return $this->IDCLIFOR;
	}
	
	/**
	 * Summary of IDMOVIMENTO
	 * @param  $IDMOVIMENTO Summary of IDMOVIMENTO
	 * @return self
	 */
	public function setIDMOVIMENTO(?int $IDMOVIMENTO): self 
    {
        if (is_string($IDMOVIMENTO)) {
            throw new LogsServiceException('O Parâmetro [IDMOVIMENTO] não pode conter um valor de tipo texto!');
        }
		$this->IDMOVIMENTO = $IDMOVIMENTO;
		return $this;
	}

	/**
	 * Summary of IDMOVIMENTO
	 * @return 
	 */
	public function getIDMOVIMENTO(): ?int {
		return $this->IDMOVIMENTO;
	}
	
	/**
	 * Summary of ACAO
	 * @param  $ACAO Summary of ACAO
	 * @return self
	 */
	public function setACAO(string $ACAO): self 
    {
        if (is_numeric($ACAO)) {
            throw new LogsServiceException('O Parâmetro [ACAO] não pode conter um valor númerico!');
        }
        
		$this->ACAO = $ACAO;
		return $this;
	}

	/**
	 * Summary of ACAO
	 * @return 
	 */
	public function getACAO(): string {
		return $this->ACAO;
	}
	
	/**
	 * Summary of MOTIVO
	 * @param  $MOTIVO Summary of MOTIVO
	 * @return self
	 */
	public function setMOTIVO(string $MOTIVO): self 
    {
        if (is_numeric($MOTIVO)) {
            throw new LogsServiceException('O Parâmetro [MOTIVO] não pode conter um valor númerico!');
        }
        
		$this->MOTIVO = $MOTIVO;
		return $this;
	}

	/**
	 * Summary of MOTIVO
	 * @return 
	 */
	public function getMOTIVO(): string {
		return $this->MOTIVO;
	}
	
	/**
	 * Summary of EMAILUSUARIO
	 * @param  $EMAILUSUARIO Summary of EMAILUSUARIO
	 * @return self
	 */
	public function setEMAILUSUARIO(string $EMAILUSUARIO): self 
    {
        if (is_numeric($EMAILUSUARIO)) {
            throw new LogsServiceException('O Parâmetro [EMAILUSUARIO] não pode conter um valor númerico!');
        }
        
		$this->EMAILUSUARIO = $EMAILUSUARIO;
		return $this;
	}

	/**
	 * Summary of EMAILUSUARIO
	 * @return 
	 */
	public function getEMAILUSUARIO(): string 
    {
		return $this->EMAILUSUARIO;
	}
	
	
    /**
	 * Summary of VALORANTIGO
	 * @param  $VALORANTIGO Summary of VALORANTIGO
	 * @return self
	 */
	public function setVALORANTIGO(?float $VALORANTIGO): self 
    {
        
        if (is_string($VALORANTIGO)) {
            throw new LogsServiceException('O Parâmetro [VALORANTIGO] não pode conter um valor de tipo texto!');
        }

		$this->VALORANTIGO = $VALORANTIGO;
		return $this;
	}
    

	/**
	 * Summary of VALORANTIGO
	 * @return 
	 */
	public function getVALORANTIGO(): ?float {
		return $this->VALORANTIGO;
	}
	
	/**
	 * Summary of VALORNOVO
	 * @param  $VALORNOVO Summary of VALORNOVO
	 * @return self
	 */
	public function setVALORNOVO(?float $VALORNOVO): self 
    {
        if (is_string($VALORNOVO)) {
            throw new LogsServiceException('O Parâmetro [VALORNOVO] não pode conter um valor de tipo texto!');
        }
        
		$this->VALORNOVO = $VALORNOVO;
		return $this;
	}

	/**
	 * Summary of VALORNOVO
	 * @return 
	 */
	public function getVALORNOVO(): ?float 
    {
		return $this->VALORNOVO;
	}
	
	/**
	 * Summary of DATAANTIGA
	 * @param  $DATAANTIGA Summary of DATAANTIGA
	 * @return self
	 */
	public function setDATAANTIGA(string $DATAANTIGA): self 
    {
        if (is_numeric($DATAANTIGA)) {
            throw new LogsServiceException('O Parâmetro [DATAANTIGA] não pode conter um valor númerico!');
        }
        
		$this->DATAANTIGA = $DATAANTIGA;
		return $this;
	}

	/**
	 * Summary of DATAANTIGA
	 * @return 
	 */
	public function getDATAANTIGA(): string 
    {
		return $this->DATAANTIGA;
	}
	
	/**
	 * Summary of DATANOVA
	 * @param  $DATANOVA Summary of DATANOVA
	 * @return self
	 */
	public function setDATANOVA(string $DATANOVA): self 
    {

        if (is_numeric($DATANOVA)) {
            throw new LogsServiceException('O Parâmetro [DATANOVA] não pode conter um valor númerico!');
        }
        
		$this->DATANOVA = $DATANOVA;
		return $this;
	}

	/**
	 * Summary of DATANOVA
	 * @return 
	 */
	public function getDATANOVA(): string 
    {
		return $this->DATANOVA;
	}
	
	/**
	 * Summary of DATAMODIFICACAO
	 * @param  $DATAMODIFICACAO Summary of DATAMODIFICACAO
	 * @return self
	 */
	public function setDATAMODIFICACAO(string $DATAMODIFICACAO): self 
    {
        if (is_numeric($DATAMODIFICACAO)) {
            throw new LogsServiceException('O Parâmetro [DATAMODIFICACAO] não pode conter um valor númerico!');
        }
        
		$this->DATAMODIFICACAO = $DATAMODIFICACAO;
		return $this;
	}

	/**
	 * Summary of DATAMODIFICACAO
	 * @return 
	 */
	public function getDATAMODIFICACAO(): string 
    {
		return $this->DATAMODIFICACAO;
	}

    public function setPostCmovimentos(array $movimento):self
    {

        if (empty($movimento)) {
            throw new LogsServiceException('O Parâmetro [movimento] não pode conter um valor vazio!');
        }

        if (!is_array($movimento)) {
            throw new LogsServiceException('O Parâmetro [movimento] tem que conter um array!');
        }

        if (is_numeric($movimento)) {
            throw new LogsServiceException('O Parâmetro [movimento] não pode conter um valor númerico!');
        }

        if (is_string($movimento)) {
            throw new LogsServiceException('O Parâmetro [movimento] não pode conter um valor de tipo texto!');
        }

        try {

            $this->movimento = $movimento;
            return $this;
        
        }catch (\Throwable $th) {

            throw new LogsServiceException("Exceção capturada:".$th->getMessage()."\n");

        }

    }
    
	public function addCmovimentos(array $movimento)
    {
        if (empty($movimento)) {
            throw new LogsServiceException('O Parâmetro [movimento] não pode conter um valor vazio!');
        }

        if (!is_array($movimento)) {
            throw new LogsServiceException('O Parâmetro [movimento] tem que conter um array!');
        }

        if (is_numeric($movimento)) {
            throw new LogsServiceException('O Parâmetro [movimento] não pode conter um valor númerico!');
        }

        if (is_string($movimento)) {
            throw new LogsServiceException('O Parâmetro [movimento] não pode conter um valor de tipo texto!');
        }

        try {
            
            $log = CmovimentosLog::create([
                'IDEMP' => $movimento['IDEMP'] ?? null,
                'IDCLIFOR' => $movimento['IDCLIFOR'] ?? null,
                'IDMOVIMENTO' => $movimento['IDMOVIMENTO']?? null,
                'ACAO' => $movimento['ACAO']?? null,
                'MOTIVO' => $movimento['MOTIVO'] ?? null,
                'EMAILUSUARIO' => $movimento['EMAILUSUARIO'] ?? null,
                'VALORANTIGO' => $movimento['VALORANTIGO'] ?? null,
                'VALORNOVO' => $movimento['VALORNOVO'] ?? null,
                'DATAANTIGA' => date($movimento['DATAANTIGA'], "Y-m-d H:i:s") ?? null,
                'DATANOVA' => date($movimento['DATANOVA'], "Y-m-d H:i:s") ?? null,
                'DATAMODIFICACAO' => date($movimento['DATAMODIFICACAO'], "Y-m-d H:i:s") ?? null,
            ]);
            $log->save();

            throw new LogsServiceException("Sucesso!");

        } catch (\Throwable $th) {

            throw new LogsServiceException("Exceção capturada: ".$th->getMessage()."\n");

        }
        
    }
	
}