<?php

namespace Solides\Business\Logs\Models\crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Summary of CitLog
 */
class CmovimentosLog extends Eloquent
{
    use HasFactory;

    /**
     * The database connection that should be used by the model.
     *
     * @var string
     */
    protected $connection = 'crm';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'CMOVIMENTOSLOG';

    /**
     * @var array
     */

     protected $fillable = [
        'IDEMP', 
        'IDCLIFOR', 
        'IDMOVIMENTO',  
        'ACAO',  
        'MOTIVO', 
        'EMAILUSUARIO', 
        'VALORANTIGO',  
        'VALORNOVO', 
        'DATAANTIGA', 
        'DATANOVA', 
        'DATAMODIFICACAO',
    ];


     /**
     * @return Attribute
     */
    public function data(): Attribute
    {
        return Attribute::make(
            get: fn($value) => json_decode($value, true),
            set: fn($value) => json_encode($value)
        );
    }


}