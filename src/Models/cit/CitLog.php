<?php

namespace Solides\Business\Logs\Models\cit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Summary of CitLog
 */
class CitLog extends Eloquent
{
    use HasFactory;
    /**
     * The database connection that should be used by the model.
     *
     * @var string
     */
    protected $connection = 'cit';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'citlog';

    /**
     * @var array
     */
    protected $fillable = [
        'acao',
        'info_msg',
        'setor',
        'request_from',
        'idclifor',
        'deal_id',
        'id_gestao',
        'status',
        'hubspot_owner_id',
        'disparado_por',
        'curl_info',
        'payload',
        'response',
        'created_at',
        'http_code',
    ];

    /**
     * @return Attribute
     */
    public function data(): Attribute
    {
        return Attribute::make(
            get: fn($value) => json_decode($value, true),
            set: fn($value) => json_encode($value)
        );
    }


}