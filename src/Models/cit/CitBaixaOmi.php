<?php

namespace Solides\Business\Logs\Models\cit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Summary of CitLog
 */
class CitBaixaOmi extends Eloquent
{
    use HasFactory;
    /**
     * The database connection that should be used by the model.
     *
     * @var string
     */
    protected $connection = 'cit';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_baixa_omie';

    /**
     * @var array
     */
    protected $fillable = [
        'status_baixa', 
        'message', 
        'idclifor',  
        'cliente',  
        'id_movimento', 
        'id_os', 
        'valor',  
        'data_vencimento_omie', 
        'nota_fiscal', 
        'documento', 
        'data_pagamento', 
        'payload_omie', 
        'created_at', 
        'updated_at', 
    ];

    /**
     * @return Attribute
     */
    public function data(): Attribute
    {
        return Attribute::make(
            get: fn($value) => json_decode($value, true),
            set: fn($value) => json_encode($value)
        );
    }


}