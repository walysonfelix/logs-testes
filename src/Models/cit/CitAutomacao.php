<?php

namespace Solides\Business\Logs\Models\cit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Summary of CitLog
 */
class CitAutomacao extends Eloquent
{
    use HasFactory;
    /**
     * The database connection that should be used by the model.
     *
     * @var string
     */
    protected $connection = 'cit';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'citlog_automacoes';

    /**
     * @var array
     */
    protected $fillable = [
        'acao',
        'status',
        'info_msg',
        'setor',
        'request_from',
        'target',
        'idclifor',
        'deal_id',
        'company_id',
        'id_gestao',
        'hubspot_owner_id',
        'curl_info',
        'http_code',
        'payload',
        'response',
        'disparado_por',
        'message_id',
        'attempts',
        'created_at',
    ];

    /**
     * @return Attribute
     */
    public function data(): Attribute
    {
        return Attribute::make(
            get: fn($value) => json_decode($value, true),
            set: fn($value) => json_encode($value)
        );
    }


}