<?php

namespace Solides\Business\Logs\Exceptions;
use Exception;

class LogsServiceException extends Exception
{
    public function __construct(string $errorMessage)
    {
        parent::__construct($errorMessage);
    }
}